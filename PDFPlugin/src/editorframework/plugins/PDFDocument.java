/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package editorframework.plugins;

import editorframework.interfaces.IDocument;
import org.apache.pdfbox.pdmodel.PDDocument;

/**
 *
 * @author aluno
 */
public class PDFDocument implements IDocument {

    PDFDocument(PDDocument pdfDocument) {
        this.pdDocument = pdfDocument;
    }
    
    public PDDocument getPDDocument() {
        return pdDocument;
    }
    
    private PDDocument pdDocument;
}
